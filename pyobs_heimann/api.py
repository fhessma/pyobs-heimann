# pyobs-heimann/api.py

"""
Data for the UDP communication with a Heimann HTPA Thermopile array camera.
"""

htpa_shapes = {
	'HTPA8x8': (8,8),
	'HTPA16x16': (16,16),
	'HTPA31x31': (31,31),
	'HTPA32x32d': (32,32),
	'HTPA64x62': (64,62),
	'HTPA80x64d': (80,64)
	}

htpa_packet_sizes = {
	'HTPA8x8': [144],
	'HTPA16x16': [548],
	'HTPA31x31': [1058,1054],
	'HTPA32x32d': [1292,1288],
	'HTPA64x62': [1101,621],
	'HTPA80x64d': [1282]
	}

htpa_packet_numbers = {
	'HTPA8x8': 1,
	'HTPA16x16': 1,
	'HTPA31x31': 2,
	'HTPA32x32d': 2,
	'HTPA64x62': 8,
	'HTPA80x64d': 10
	}

htpa_device_types = {
	'0':  'HTPA8x8',
	'1':  'HTPA16x16',
	'3':  'HTPA31x31',
	'10': 'HTPA32x32d',
	'5':  'HTPA64x62',
	'11': 'HTPA80x64d'
	}

htpa_dictionary = {
	'ADCRES':	'ADC resolution',
	'AT':		'array type number',
	'BIAS':		'bias level',
	'BPA':		'?',
	'DEVID':	'device id',
	'IP':		'network address',
	'MACID':	'network MAC id',
	'MCLK':		'clock rate [kHz]',
	'MODT':		'module type',
	'MSK':		'network mask',
	'PU':		'?',
	'REFCAL':	'reference calibration?',
	'VDD':		'voltage [VDC]'
	}

htpa_80x64_packet_bounds = {
	'image': [0,5119],
	'offset': [5120,6399],
	'VDD': [6400],
	'TAMB': [6401],
	'PTAT': [6402,6409],
	'data': {
		1:[0,640], 2:[641,1281], 3:[1282,1922], 4:[1923,2563],
		5:[2564,3204], 6:[3205,3845], 7:[3846,4486], 8: [4487,5119]
		}
}

htpa_commands = {
	'bind_device': {
		'cmd':    'Bind HTPA series device',
		'answer': 'HW-Filter released\r\n',
		'help':   'Tells device to enable hardware IP filter and accept control character commands.'
		},
	'broadcast': {
		'cmd':    'Calling HTPA series devices',
		'answer': 'HTPA series responsed! I am Arraytype {0} MODTYPE {1}\r\nADC: {2}\r\n', 
		'help':   'broadcast for finding HTPA series devices'
		},
	'calibrate': {
		'cmd':    'W',
		'answer': None,
		'help':   'calibrate (overwrites old calibration data!)'
		},
	'decrease_bias': {
		'cmd':    'i',
		'answer': None,
		'help':   'decrease the value of the bias'
		},
	'decrease_bpa': {
		'cmd':    'j',
		'answer': None,
		'help':   'decrease the value of the BPA'
		},
	'decrease_clock': {
		'cmd':    'a',
		'answer': None,
		'help':   'decrease the value of the CLK setting'
		},
	'decrease_refcal': {
		'cmd':    'o',
		'answer': None,
		'help':   'decrease the value of the REFCAL setting'
		},
	'decrease_resolution': {
		'cmd':    'r',
		'answer': None,
		'help':   'decrease the resolution'
		},
	'increase_bias': {
		'cmd':    'I',
		'answer': None,
		'help':   'increase the value of the bias'
		},
	'increase_bpa': {
		'cmd':    'J',
		'answer': None,
		'help':   'increase the value of the BPA'
		},
	'increase_clock': {
		'cmd':    'A',
		'answer': None,
		'help':   'increase the value of the CLK setting'
		},
	'increase_refcal': {
		'cmd':    'O',
		'answer': None,
		'help':   'increase the value of the REFCAL setting'
		},
	'increase_resolution': {
		'cmd':    'R',
		'answer': None,
		'help':   'increase the resolution'
		},
	'get_config': {
		'cmd':    'G',
		'answer': None,
		'help':   'get current voltage configuration'
		},
	'get_ip': {
		'cmd':    'v',
		'answer': None,
		'help':   'announce IP'
		},
	'get_timage' : {
		'cmd':    'k',
		'answer': None,
		'help':   'get a single  temperature frame'
		},
	'get_vdd': {
		'cmd':    'b',
		'answer': None,
		'help':   'measure VDD referencesd to VREF1225'
		},
	'get_vimage' : {
		'cmd': 'c',
		'answer': None,
		'help':   'get a single voltage frame'
		},
	'release_calibration': {
		'cmd':    'q',
		'answer': None,
		'help':   'enables calibration'
		},
	'release_device': {
		'cmd':    'Release HTPA series device',
		'answer': 'HW-Filter released\r\n',
		'help':   'Tells device to disable hardware IP filter.'
		},
	'set_id': {
		'cmd':    'Set DeviceID to {0}',
		'answer': 'DeviceID changed to {0}\r\n',
		'help':   'Tells device to change its name to something other than its array type.'
		},
	'set_network': {
		'cmd':    'HTPA device IP change request to {0}.{1}.',
		'answer': 'Device changed IP to {0}. and Subnet to {1}.\r\n',
		'help':   'Tells device to change the IP and subnet mask defaults and to save them in EEPROM.'
		},
	'start_tstream' : {
		'cmd':    'K',
		'answer': None,
		'help':   'start a continuous strema of temperature frames'
		},
	'start_vstream' : {
		'cmd':    't',
		'answer': None,
		'help':   'start a continuous stream of voltage frames'
		},
	'status' : {
		'cmd': 'M',
		'answer': None,
		'help':   'shows current settings'
		},
	'stop_stream' : {
		'cmd': 'x',
		'answer': None,
		'help':   'stops stream without prompt'
		},
	'toggle_raw' : {
		'cmd': 'f',
		'answer': None,
		'help': 'toggle between sending raw and compensated voltages'
		},
	'toggle_pu' : {
		'cmd': 'p',
		'answer': None,
		'help': 'toggle pull-up setting'
		},
}	# MISSING : 'Set EEPROM data' !!!

