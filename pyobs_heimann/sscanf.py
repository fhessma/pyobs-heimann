# sscanf.py

def stripped_format (fmt) :
	"""
	Takes a perfectly reasonable python format string and removes the size info so that
	parse.parse can read values using it (like C's "sscanf").
	Any reasonable language having str.format would have the reverse operation..... :-(

	Example:  "COE{0:03d}_{1:1d}"...
	"""
	parts = fmt.split('{')	# e.g. ["COE","0:03d}_","1:1d}"]
	if len(parts) == 1 :
		raise ValueError ('string is not a format: {0}'.format(fmt))
	f = parts[0]
	for i in range(1,len(parts)) :
		if '}' not in parts[i] :	# WHOOPS!
			raise ValueError ('no matching {} in format!')
		things = parts[i].split('}')		# e.g. ["0:03d","_"]
		stuff = things[0].split(':')		# e.g. ["0","03d"]
		f += '{:'+stuff[1]+'}'
		if len(things) == 2 :
			f += things[1]
	return f

def sscanf (fmt,s) :
	"""
	Rough equivalent of C's sscanf using parse.parse.
	Returns a list of values extracted using a format string.
	"""
	return parse.parse (stripped_format(fmt),s).fixed


