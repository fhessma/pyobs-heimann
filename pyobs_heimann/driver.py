# driver.py

"""
UDP communications with a Heimann HTPA Thermopile array camera.

"Streaming" is a continuous averaging readout of the array, optinally dumped to
FITS image files.
"""

import binascii
import logging
import numpy as np
import os
import socket
import struct
import sys
import threading
import time

from astropy.io import fits
from api import htpa_commands, htpa_80x64_packet_bounds, htpa_dictionary
from sscanf import sscanf

logging.basicConfig (level=logging.INFO,format='%(asctime)s * %(levelname)s * %(message)s')


class HeimannSensor (object) :
	"""
	Note: port=30444 is defined by the software in Heimann's cameras.
	"""
	def __init__ (self, port=30444, *args, **kwargs) :
		self._port = port
		self._lock = threading.Lock()
		self._socket   = None
		self._raw      = None				# 1-D IMAGE DATA COMING IN IN CHUNKS
		self._image    = None				# FINAL 2-D IMAGE
		self._address  = None				# IP ADDRESS OF DEVICE
		self._arrtype  = None				# STRING ARRAY TYPE

		# AVERAGING/STREAMING VARIABLES
		self._streamer    = None			# THREAD FOR STREAMING
		self.is_exposing  = False			# WHETHER CURRENTLY MAKING A SINGLE AVERAGED EXPOSURE
		self.is_streaming = False			# WHETHER CURRENTLY STREAMING
		self._exptime     = None			# AVERAGE TIMESCALE
		self._nimages     = None			# NUMBER OF IMAGES IN AVERAGE
		self._storage     = None			# FOR COMPUTING AVERAGE IMAGE
		self._starttime   = None			# STARTING UTC OF LAST AVERAGE
		self._nstreamed   = 0				# INDEX NO. OF STREAMED IMAGES
		self._stdev       = None			# MEAN STDEV OF AVERAGE IMAGE

		# INITIALIZE STATUS DICTIONARY
		self.status = {
			'shape': None,
			'stdev': None,
			'AT': None,
			'MCLK': None,
			'VDD': None,
			'BIAS': None,
			'BPA': None,
			'PU': None,
			'REFCAL': None,
			'MODT': None,
			'ADCRES': None,
			'IP': None,
			'MACID': None,
			'DEVID': None,
			'MSK': None
			}

		# BIND A SOCKET TO THE PORT
		self._socket = socket.socket (socket.AF_INET,socket.SOCK_DGRAM)		# UDP
		self._socket.setsockopt (socket.SOL_SOCKET,socket.SO_BROADCAST,1)	# BROADCAST
		self._socket.bind (('localhost',self._port))

		# START LISTENING VIA A THREAD
		self.listener = threading.Thread (target=self.listen)
		self.listener.start ()

	def disconnect (self) :
		logging.info ('joining thread and closing socket ...')
		if self.listener is not None :
			self.listener.join()
		if self.streamer is not None :
			self.streamer.join()
		self._socket.close ()

	def send_command (self, command, *args) :
		"""
		Send a command in htpa_commands to the device.
		"""
		if (self._address is not None) and (command in htpa_commands) :
			cmd = htpa_commands[command]
			if '{' in cmd :
				cmd = htpa_commands[command].format(*args)
			l = len(cmd)
			s = struct.Struct ('{0}s'.format(l))
			packet = s.pack((cmd))
			self._socket.sendto (packet,self._address)

	def listen (self) :
		"""
		Find a device and start listening to the communication.
		"""
		ibroadcast = 0
		while True :

			# NO ACTIVE CONNECTION?
			if self._arrtype is None :
				if ibroadcast == 0 :
					logging.info ('broadcasting to find device ...')
					self.send_command ('broadcast')
				time.sleep (1.0)
				ibroadcast += 1
				if ibroadcast > 20 :
					logging.info ('waiting for connection ...')
					ibroadcast=0

			# LISTEN FOR INCOMING MESSAGES
			else :
				self._socket.settimeout (1.0)	# MAKE SMALLER!!!!
				try :
					data,address = self._socket.recvfrom (4096)	# MAKE BIGGER????

					logging.debug ('Received {} bytes from {}'.format(len(data),address))
					nbytes = min(len(data),16)
					d = data[:nbytes]
					logging.info ('  as string : {0} ...'.format(str(d)))
					logging.info ('  as hex    : {0} ...'.format(hexlify(d)))
					self.parse_packet (data,address)
				except socket.timeout :
					pass

	def parse_packet (self, data, address) :
		"""
		Processes data received as a UDP packet.
		"""
		nd = len(data)
		sdata = str(data)

		# PARSE IMAGE DATA
		if nd == 144                    and self._arrtype == 'HTPA8x8' :
			self.parse_8x8_frame_packet (data,nd)
		elif nd == 548                  and self._arrtype == 'HTPA16x16' :
			self.parse_16x16_frame_packet (data,nd)
		elif (nd == 1058 or nd == 1054) and self._arrtype == 'HTPA32x31' :
			self.parse_32x31_frame_packet (data,nd)
		elif (nd == 1292 or nd == 1288) and self._arrtype == 'HTPA32x32d' :
			self.parse_32x32_frame_packet (data,nd)
		elif (nd == 1101 or nd == 621)  and self._arrtype == 'HTPA64x62' :
			self.parse_65x62_frame_packet (data,nd)
		elif nd == 1283                 and self._arrtype == 'HTPA80x64d' :
			self.parse_80x64_frame_packet (data,nd)

		# PROCESS THE RESPONSE TO STANDARD COMMANDS
		else :
			for cmd in htpa_commands :
				answer = cmd['answer']
				ans = answer
				if answer is not None :
					if '{' in answer :
						ans = answer[:answer.index('{')]
					if sdata.startswith(ans) :
						logging.debug ('received response to command {0}'.format(cmd))

						# RESPONSE TO BROADCAST?
						if 'I am Arraytype' in sdata :
							if self._address is None :
								with self.lock :
									logging.warning ('hard-wired to HTPA80x64d!')
									self._socket.setsockopt (socket.SOL_SOCKET,socket.SO_BROADCAST,0)	# NO BROADCAST
									self._address = address
									arrtype = 'HTPA80x64d'
									self._arrtype = arrtype
									self.status['AT'] = 11
									self.status['shape'] = (80,64)

	def parse_8x8_frame_packet (self, data, nbytes) :
		logging.critical ('8x8 frame packet not supported!')
	def parse_16x16_frame_packet (self, data, nbytes) :
		logging.critical ('16x16 frame packet not supported!')
	def parse_32x31_frame_packet (self, data, nbytes) :
		logging.critical ('32x31 frame packet not supported!')
	def parse_32x32_frame_packet (self, data, nbytes) :
		logging.critical ('32x32 frame packet not supported!')
	def parse_64x62_frame_packet (self, data, nbytes) :
		logging.critical ('64x62 frame packet not supported!')

	def parse_80x64_frame_packet (self, data, nbytes) :
		"""
		Parse a HTPA80x64 UDP frame packet consisting of an 8bit packet index followed by 641 image
		values (0-640 for the 0th packet, 641-1281 for the 2nd,...), or, for packets #8-#10,
		offset values (see...).
		"""
		bounds = htpa_80x64_packet_bounds['data']

		pindx,pdata = struct.unpack ('< B 641*h',data)	# UNSIGNED BYTE AND THEN 641 LOW-ENDIAN 16BIT VALUES
		logging.debug ('parsed block #{0}'.format(pindx))

		starttime = None
		with self.lock :
			if pindx == 1 :
				self._raw = np.zeros(self.status['shape'])
				starttime = str(datetime.datetime.now())
			if pindx >= 1 and pindx <= 8 :
				i1,i2 = bounds[pindx]
				self._raw[i1:i2] = pdata

			# GET OFFSET IMAGE...
			if pindx >= 8 and pindx <= 10 :
				pass

			# PACKET STEAM FINISHED?
			if pindx == 10 :
				self._raw = None
				self._starttime = starttime

				# STREAMING/EXPOSING: PUT IMAGE WITHIN STORAGE ARRAY
				if self.is_streaming or self.is_exposing :
					if self._nstreamed > 0 :
						self._nstreamed -= 1
						self._storage[self._nstreamed,:,:] = self._raw.reshape(self.status['shape'])

					# STOP IF MAKING SINGLE AVERGING EXPOSURE
					if self._nstreamed == 0 and self.is_exposing :
						self.is_exposing = False
						self._image = self.mean_image ()

				# JUST PUT A SINGLE IMAGE IN "AVERAGE"
				else :
					self._image = self._raw.reshape(self.status['shape'])

	def has_image (self) :
		return self._image is not None

	def get_image (self) :
		""" Returns a copy of the sensor's mean image """
		with self.lock :
			return np.array(np.nanmean(self._image))

	def start_stream (self, exptime=1., pathname=None, show=True) :
		"""
		Starts a steady stream of exposures averaged over the time "exptime".
		If "pathname" is given, the images are stored as FITS files in the
		directory path given.
		"""
		self.is_streaming = True
		self._exptime = exptime
		self._directory = pathname
		self._streamer = threading.Thread (target=self.streaming)
		self._streamer.start()

	def streaming (self) :
		""" Thread method for streaming """

		# CREATE DIRECTORY?
		if (self._directory is not None) and (not os.path.isdir (self._directory)) :
			try :
				os.mkdir (self._directory)
			except OSError :
				logging.error ('cannot use directory {0}'.format(self._directory))
				self.join()
				return

		# NUMBER OF IMAGES IN EXPTIME?
		nexp = 10
		logging.warning ('ignoring exptime for now: simply averaging 10 exposures')

		# START AVERAGING
		with self.lock :
			self._nimages = nexp	# NUMBER OF LAST IMAGE STORED
			shape = self.status['shape']
			self._storage = np.full((nexp,shape[0],shape[1]),np.nan)
			self._image = None

		while self.is_streaming :
			# NEXT IMAGE READY?
			if self._nimages == 0 :

				# CALCULATE AVERAGE
				with self.lock :
					self._image = self.mean_image ()
					self._nimages = nexp		# GET READY FOR NEXT IMAGE
					self._nstreamed += 1

				# STORE?
				if self._directory is not None :
					hdu = fits.PrimaryHDU (self._image)
					info = {'stdev':(self._stdev,'mean std.dev. of image')}
					self.add_header (hdu, info=info)
					pathname = self._directory+'/htpa_'+'{0:04d}.fits'.format(self._nstreamed)
					hdu.writeto (pathname,overwrite=False)

	def mean_image (self) :
		self._image = np.nanmedian (self._storage,axis=1)
		stdev_img = np.std (self._storage,axis=2)
		self._stdev = np.mean(stdev_img)

	def add_header (self, hdu, info=None) :
		""" Adds current info to an empty FITS HDU header """
		hdr = hdu.header
		hdr['UDP_PORT'] = (self._port,'port for UDP communications')
		hdr['DATE-OBS'] = (self._starttime,'starting UTC datetime')
		for key,val in self.status.items() :
			if val is not None :
				if key in htpa_dictionary :
					hdr[val] = (val,htpa_dictionary[val])
				else :
					hdr[val] = val
		if info is not None :
			for key,val in info.items() :
				if val is not None :
					hdr[val] = val

	def stop_stream (self) :
		""" Stops an already started stream of images. """
		with self.lock :
			self.is_streaming = False

if __name__ == '__main__' :

	def help () :
		print ('Commands (most based directly on the Heimann UDP commands) :')
		for key,val in htpa_commands.items() :
			print ('\t{0:20s}\t{1}'.format(key,val['help']))
		print ('\t{0:20s}\tdisplay a transferred image'.format('show'))
		print ('\thelp')
		print ('\t{q}uit\n')

	sensor = HeimannSensor ()
	ok = True
	transID = 1
	print ('\nHeimann HTPA test programme, Version 0.1\n==================================\n')
	help()
	try :
		while (ok) :
			# NEXT COMMAND
			cmd = input ('{0:02d}> '.format(transID))
			parts = cmd.split()
			if len(parts) > 1 : cmd = parts[0]

			# GET ANY INFO ABOUT THE COMMAND
			if cmd in htpa_commands :
				info = htpa_commands[cmd]
			else :
				info = {}

			# PROCESS COMMAND
			if cmd.startswith('q') :
				ok = False
			elif cmd == 'help' :
				help()
			elif cmd == 'continuous' :
				texp = 1. # SEC
				dirname = None
				if len(parts) >= 1 :
					texp = float(parts[1])
				if len(parts) == 2 :
					dirname = parts[2]
				img = sensor.start_stream (exptime=texp,pathname=dirname,show=True)
			elif cmd == 'show' :
				if sensor.has_image () :
					img = sensor.get_image ()
					plt.imshow (img)
					plt.show ()
				else :
					print ('Nothing to show!')
			elif cmd == 'stop' :
				sensor.stop_stream ()
			elif cmd in htpa_commands and 'cmd' in info :
				cmdargs = parts[1:]
				sensor.send_command (cmd,cmdargs)
			elif (cmd != '') and (cmd not in htpa_commands) :
				print ('Not a command!')

			transID += 1
			if transID > 99 : transID = 1

	except KeyboardInterrupt :
		ok = False

	print ('Disconnecting sensor...')
	sensor.disconnect()
	sys.exit(0)
