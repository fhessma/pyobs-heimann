# pyobs-heimann

This project contains a pyobs driver for cloud detectors using the Heimann Thermopile array sensor cameras.

The driver.py script can also be used to test the Heimann UDP-based interface to their "Starter-Kit" camera.